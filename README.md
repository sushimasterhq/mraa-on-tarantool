# Our Youtube Channel:

SushiMaster Channel: https://youtube.com/c/SushiMasterOfficial

# Install Tarantool on Ubuntu

Install tarantool on your Ubuntu (Baremetal/WSL/VM):

Ubuntu: https://www.tarantool.io/en/download/os-installation/ubuntu/

## Add mraa lib files to your ubuntu

sudo mkdir /usr/lib/x86_64-linux-gnu/pkgconfig

sudo mkdir /usr/local/share/lua

sudo mkdir /usr/local/share/lua/5.1

cd ~

git clone https://gitlab.com/sushimasterhq/mraa-on-tarantool.git

cd mraa-on-tarantool

sudo cp usr/lib/x86_64-linux-gnu/pkgconfig/mraa.pc /usr/lib/x86_64-linux-gnu/pkgconfig/

sudo cp usr/lib/x86_64-linux-gnu/libmraa.so.2.2.0 /usr/lib/x86_64-linux-gnu/

sudo cp usr/lib/x86_64-linux-gnu/libmraa.so.2 /usr/lib/x86_64-linux-gnu/

sudo cp usr/lib/x86_64-linux-gnu/libmraa.so /usr/lib/x86_64-linux-gnu/

sudo cp usr/local/share/lua/5.1/mraa.so /usr/local/share/lua/5.1/


